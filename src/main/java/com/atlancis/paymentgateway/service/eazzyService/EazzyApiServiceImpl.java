/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.atlancis.paymentgateway.service.eazzyService;

import com.atlancis.paymentgateway.configuration.EazzyConfig;
import com.atlancis.paymentgateway.service.mpesaService.MpesaApiServiceImpl;
import java.io.UnsupportedEncodingException;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author nthemba
 */
@Service
public class EazzyApiServiceImpl implements EazzyApiService{
    
    private Logger log =  Logger.getLogger(EazzyApiServiceImpl.class.getName());

    
    @Autowired
    EazzyConfig eazzyConfig;

    @Override
    public String getAuthToken() {
        String token = null;
        try {
            token = generateEncodedString();
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(EazzyApiServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        String url = "https://api-test.equitybankgroup.com/identity-uat/v1/token";
        //https://api-test.equitybankgroup.com/v1/token
        //https://api-test.equitybankgroup.com/identity-uat/v1/token
        RestTemplate restTemplate =  new RestTemplate();
        
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        headers.add("Authorization", "Basic " + token);
        
        MultiValueMap<String, String> map= new LinkedMultiValueMap<>();
        map.add("username", eazzyConfig.getMerchantCode());
        map.add("password", eazzyConfig.getMerchantKey());
        map.add("grant_type", "password");
        map.add("merchant","merchant");
        
        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);
        
        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, request, String.class);
        
        log.log(Level.INFO, response.toString());

        log.log(Level.INFO, response.getBody());

        return response.getBody();
    }
 
    private String generateEncodedString() throws UnsupportedEncodingException {
       String key =  eazzyConfig.getConsumerKey();
       String secret = eazzyConfig.getConsumerSecret();
       
       String appKeySecret = key + ":" + secret;
       byte[] bytes = appKeySecret.getBytes("UTF-8");
       String auth = Base64.getEncoder().encodeToString(bytes);
       
       log.log(Level.INFO, auth);
       return auth;

    }
    
}
