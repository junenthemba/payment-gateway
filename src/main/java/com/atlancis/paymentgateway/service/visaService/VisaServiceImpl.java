/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.atlancis.paymentgateway.service.visaService;

import com.atlancis.paymentgateway.utils.XPayTokenGenerator;
import java.security.SignatureException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang3.RandomStringUtils;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author nthemba
 */
@Service
public class VisaServiceImpl implements VisaService {

    private Logger log =Logger.getLogger(VisaServiceImpl.class.getName());
    @Override
    public String generateVisaKey() {  
        
         String apiKey = "QCGM6WFM5X1QFHVUF61N21ZrbmsyVnwjJtEQGhr2OM8C-VD5M";
         String url = "https://sandbox.api.visa.com/cybersource/payments/flex/v1/keys?apikey=" + apiKey;
         String resourcePath = "payments/flex/v1/keys";
        
        JSONObject requestBody = new JSONObject();
        requestBody.put("encryptionType", "RsaOaep256");
        
        String token= null;
        try {
            token = XPayTokenGenerator.generateXpaytoken(resourcePath, apiKey, requestBody.toString());
        } catch (SignatureException ex) {
            Logger.getLogger(VisaServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        RestTemplate restTemplate =  new RestTemplate();
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
        
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("x-pay-token", token);
        headers.add("ex-correlation-id", getCorrelationId());

        
        HttpEntity<String> request = new HttpEntity<>(requestBody.toString(),headers);
        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, request, String.class);
        return response.getBody();
    }
    
    private String getCorrelationId() {
        //Passing correlation Id header is optional while making an API call. 
        return RandomStringUtils.random(10, true, true) + "_SC";
    }
    
}
