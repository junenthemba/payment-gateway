/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.atlancis.paymentgateway.service.dataservice;

import com.atlancis.paymentgateway.customEntitites.B2BRequest;
import com.atlancis.paymentgateway.customEntitites.C2bResponse;
import com.atlancis.paymentgateway.customEntitites.LNMRequest;
import com.atlancis.paymentgateway.model.B2bMpesaRequest;
import com.atlancis.paymentgateway.model.B2bTransaction;
import com.atlancis.paymentgateway.model.LnmMpesaTransaction;
import com.atlancis.paymentgateway.model.C2bTransaction;
import com.atlancis.paymentgateway.model.LnmMpesaRequest;
import com.atlancis.paymentgateway.model.MpesaRequest;
import com.atlancis.paymentgateway.service.mpesaService.MpesaTransactionType;
import com.atlancis.paymentgateway.service.mpesaService.RequestStatus;
import java.util.List;


/**
 *
 * @author nthemba
 */

public interface DataService {
    
    C2bTransaction saveC2BTransaction(C2bTransaction c2bTransaction);
    
    List<C2bTransaction> listC2bTransaction();
    
    C2bTransaction getC2bTransactionByTransId(String TransID);
    
    List<C2bTransaction> getC2bTransactionByPhoneNumber(String phoneNumber);
    
    void mapToC2bTransaction(String response);
    
    void mapToC2bTransaction(C2bResponse c2bResponse, String ipAddress);
    
    
    LnmMpesaTransaction saveLnmMpesaTransaction(LnmMpesaTransaction lnmMpesaTransaction);
    
    List<LnmMpesaTransaction> listLnmMpesaTransaction();
    
    LnmMpesaTransaction getLnmMpesaTransactionByTransId(String transId);
    
    List<LnmMpesaTransaction> getLnmMpesaTransactionByPhoneNumber(String phoneNumber);
    
    void mapToLnmMpesaTransaction(String response, String ipAddress);
    
    
    B2bTransaction saveB2bTransaction(B2bTransaction b2bTransaction);
    
    List<B2bTransaction> listB2bTransaction();
    
    B2bTransaction getB2bTransactionByTransactionId(String transactionID);
    
    void mapToB2bTransaction(String response, String ipAddress);
    
    
    MpesaRequest saveMpesaRequest(MpesaRequest mpesaRequest);
    
    List<MpesaRequest> listMpesaRequest();
    
    void mpesaRequestHelper(String ipAddress, String requestDetails, RequestStatus status, MpesaTransactionType requestType, String requestResult);
    
    
    LnmMpesaRequest saveLnmMpesaRequest(LnmMpesaRequest lnmMpesaRequest);
    
    void lnmMpesaRequestHelper(String ipAddress,LNMRequest lnmRequest, String response, RequestStatus requestStatus);
    
    List<LnmMpesaRequest> findLnmMpesaRequestByAccountRef(String accountRef);
    
    String getResultUrl(String merchantRequestId, String checkoutRequestId);
    
    LnmMpesaRequest getLnmMpesaRequestByMerchantAndCheckoutId(String merchantRequestId, String checkoutRequestId);
    
            
    B2bMpesaRequest saveB2bMpesaRequest(B2bMpesaRequest b2bMpesaRequest);
    
    void b2bMpesaRequestHelper(String ipAddress, B2BRequest b2BRequest, String response, RequestStatus requestStatus);
    
    List<B2bMpesaRequest> findB2bMpesaRequestByAccountRef(String accountRef);
    
    B2bMpesaRequest findB2bMpesaRequestByOriginatorConversationAndConversationID(String originatorConversationID, String conversationID);
    
   
}
