/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.atlancis.paymentgateway.service.mpesaService;

import java.io.UnsupportedEncodingException;
import org.json.JSONObject;

/**
 *
 * @author nthemba
 */
public interface MpesaApiService {
        
    String sendRequest(String url, String requestBody);
    
    String sendMpesaResponse(String url, String mpeseResponseBody);
    
//    String simulateC2B(String url , String requestBody);
//    
//    String lipaNaMpesaOnline(String url, String requestBody);
//        
//    String B2BPaymentRequest(String url, String requestBody);
//    
//    String B2CPaymentRequest(String url, String requestBody);
//    
//    String accountBalance (String url, String requestBody);
//    
//    String getTRansactionStatus(String url, String requestBody);
}
