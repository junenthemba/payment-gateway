/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.atlancis.paymentgateway.service.mpesaService;

import com.atlancis.paymentgateway.configuration.MpesaConfig;
import com.atlancis.paymentgateway.customEntitites.B2BRequest;
import com.atlancis.paymentgateway.customEntitites.LNMRequest;
import com.atlancis.paymentgateway.utils.PaymentUtils;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author nthemba
 */
@Service
public class MpesaRequestServiceImpl implements MpesaRequestService{
    
    @Autowired MpesaConfig yamlConfig;
    
    Logger log = Logger.getLogger(MpesaRequestServiceImpl.class.getName());
    
    @Override
    public String lipaNaMpesaRequest(JSONObject json){
        
        Date now = new Date();
        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMddHHmmss");
        String today = dateFormatter.format(now);
        
        String password = encodeLNPPassword(today);
        
        String formatedPhoneNumber = PaymentUtils.formatPhoneNumber(json.optString("phoneNumber"));

        JSONObject request = new JSONObject();
        request.put("BusinessShortCode",yamlConfig.getLnmShortcode());
        request.put("Password", password);
        request.put("Timestamp", today);
        request.put("TransactionType", "CustomerPayBillOnline");
        request.put("Amount", json.optDouble("amount"));
        request.put("PartyA", formatedPhoneNumber);
        request.put("PartyB", yamlConfig.getLnmShortcode());
        request.put("PhoneNumber", formatedPhoneNumber);
        request.put("CallBackURL", yamlConfig.getNgrok() + yamlConfig.getLnmCallbackUrl());
        request.put("AccountReference", json.optString("accRef"));
        request.put("TransactionDesc", json.optString("transactionDesc"));
        
        log.log(Level.SEVERE, request.toString());

        return request.toString();
        
    }
    
    @Override
    public String lipaNaMpesaRequest(LNMRequest request) {
        Date now = new Date();
        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMddHHmmss");
        String today = dateFormatter.format(now);
        
        String password = encodeLNPPassword(today);
        
        String formatedPhoneNumber = PaymentUtils.formatPhoneNumber(request.getPhoneNumber());

        JSONObject requestBody = new JSONObject();
        requestBody.put("BusinessShortCode",yamlConfig.getLnmShortcode());
        requestBody.put("Password", password);
        requestBody.put("Timestamp", today);
        requestBody.put("TransactionType", "CustomerPayBillOnline");
        requestBody.put("Amount", request.getAmount());
        requestBody.put("PartyA", formatedPhoneNumber);
        requestBody.put("PartyB", yamlConfig.getLnmShortcode());
        requestBody.put("PhoneNumber", formatedPhoneNumber);
        requestBody.put("CallBackURL", yamlConfig.getNgrok() + yamlConfig.getLnmCallbackUrl());
        requestBody.put("AccountReference", request.getAccRef());
        requestBody.put("TransactionDesc", request.getTransactionDesc());
        
        log.log(Level.SEVERE, requestBody.toString());

        return requestBody.toString();
    }
    
    public String encodeLNPPassword(String today) {
        String passkey = yamlConfig.getPasskey();
        String shortcode = yamlConfig.getLnmShortcode();
        
        String passkeyDate = shortcode + passkey + today;
        byte[] bytes = null;
        try {
            bytes = passkeyDate.getBytes("UTF-8");
        } catch (UnsupportedEncodingException ex) {
            log.log(Level.SEVERE, null, ex);
        }
        
        return Base64.getEncoder().encodeToString(bytes);
    }

    @Override
    public String registerUrlRequest() {
        String confirmUrl = yamlConfig.getNgrok() + yamlConfig.getC2bConfirmUrl();
        String validationUrl = yamlConfig.getNgrok() + yamlConfig.getC2bValidationUrl();
        
        JSONObject request = new JSONObject();
        request.put("ShortCode", yamlConfig.getShortcodePartyA());
        request.put("ResponseType", "Completed");
        request.put("ConfirmationURL", confirmUrl);
        request.put("ValidationURL", validationUrl);
        
        log.info(request.toString());
        
        return request.toString();
        
    }

    // Acheck has to be done to verify we are expecting money from this client
    @Override
    public String c2bConfirmation() {
        
        JSONObject response = new JSONObject();
        response.put("ResultCode", 0);
        response.put("ResultDesc", "The service was saved successfully");
        
        return response.toString();
    }

    // make sure that the record has been saved to the db before returning response
    @Override
    public String c2bValidate() {
        
        JSONObject response = new JSONObject();
        response.put("ResultCode", 0);
        response.put("ResultDesc", "The service was saved successfully");
        
        return response.toString();
    }

    //This is just a simulation of a customer initiated c2b
    //Not used in production
    @Override
    public String c2bSimulate(String phoneNumber, Double amount) {
        
        String shortcode = yamlConfig.getShortcodePartyA();
        
        JSONObject request = new JSONObject();
        request.put("CommandID", "CustomerPayBillOnline");
        request.put("BillRefNumber", phoneNumber);
        request.put("ShortCode", shortcode);
        request.put("Msisdn", phoneNumber);
        request.put("Amount", amount);
        
        return request.toString();
    }

    @Override
    public String b2bPaymentRequest(String shortcode, Double amount) {
        
        String queueTimeoutUrl = yamlConfig.getNgrok() + yamlConfig.getB2bQueueTimeOutUrl();
        String resultUrl = yamlConfig.getNgrok() + yamlConfig.getB2bResultUrl();

        JSONObject request = new JSONObject();
        request.put("Initiator", "TestInit602");
        request.put("SecurityCredential", "XAj8xw5mZZ3X6jTcJnKseSkk43AO5aWi36BSrbCtLIAtkRWWmbhL169/kJIa/0W1kvrBqBCCFXWJdDwD65/jL0r8Bmsl5yzXMalvErsdyAzIL1ySoeusiR0D5GCNl+Q5aC134toomGQzILZvDw6otUh3/eLgycOE2UA/7LxcccwV9LgQg69NwZFzX3r9oSpwhdSfMP+/dMdvngK1zxN3w6lrstfs88jUcWgaq2vpbgAuZdD8jaJ7LvhwjttLm0ILdc3ZzyYjk9KXMzM2QLl7zNIVWRKwsLshIXC/MDOAVd1j26I+vPsokSh83u/mihKzabVVAsaT4xMGGCMxqZfgEg==");
        request.put("CommandID", "BusinessToBusinessTransfer");
        request.put("SenderIdentifierType", yamlConfig.getSenderIdentifierOrganizationShortcode());
        request.put("RecieverIdentifierType", yamlConfig.getSenderIdentifierOrganizationShortcode());
        request.put("Amount", amount);
        request.put("PartyA", 600602);
        request.put("PartyB",shortcode );
        request.put("AccountReference", "siNnZQgdpK");
        request.put("Remarks", "payment");
        request.put("QueueTimeOutURL", queueTimeoutUrl);
        request.put("ResultURL", resultUrl);
        
        return request.toString();
    }
    
    @Override
    public String b2bPaymentRequest(B2BRequest b2BRequest) {
        
        String queueTimeoutUrl = yamlConfig.getNgrok() + yamlConfig.getB2bQueueTimeOutUrl();
        String resultUrl = yamlConfig.getNgrok() + yamlConfig.getB2bResultUrl();

        JSONObject request = new JSONObject();
        request.put("Initiator", "TestInit602");
        request.put("SecurityCredential", "XAj8xw5mZZ3X6jTcJnKseSkk43AO5aWi36BSrbCtLIAtkRWWmbhL169/kJIa/0W1kvrBqBCCFXWJdDwD65/jL0r8Bmsl5yzXMalvErsdyAzIL1ySoeusiR0D5GCNl+Q5aC134toomGQzILZvDw6otUh3/eLgycOE2UA/7LxcccwV9LgQg69NwZFzX3r9oSpwhdSfMP+/dMdvngK1zxN3w6lrstfs88jUcWgaq2vpbgAuZdD8jaJ7LvhwjttLm0ILdc3ZzyYjk9KXMzM2QLl7zNIVWRKwsLshIXC/MDOAVd1j26I+vPsokSh83u/mihKzabVVAsaT4xMGGCMxqZfgEg==");
        request.put("CommandID", "BusinessToBusinessTransfer");
        request.put("SenderIdentifierType", yamlConfig.getSenderIdentifierOrganizationShortcode());
        request.put("RecieverIdentifierType", yamlConfig.getSenderIdentifierOrganizationShortcode());
        request.put("Amount", b2BRequest.getAmount());
        request.put("PartyA", 600602);
        request.put("PartyB",b2BRequest.getShortCode() );
        request.put("AccountReference", b2BRequest.getAccRef());
        request.put("Remarks", b2BRequest.getRemarks());
        request.put("QueueTimeOutURL", queueTimeoutUrl);
        request.put("ResultURL", resultUrl);
        
        return request.toString();
        
    }

    @Override
    public String b2cPaymentRequest(String phoneNumber, Double amount) {
        
        String queueTimeoutUrl = yamlConfig.getNgrok() + yamlConfig.getB2cQueueTimeOutUrl();
        String resultUrl = yamlConfig.getNgrok() + yamlConfig.getB2cResultUrl();
        
        JSONObject request = new JSONObject();
        request.put("InitiatorName", yamlConfig.getInitiator());
        request.put("SecurityCredential", yamlConfig.getSecurityCredential());
        request.put("CommandID", "PromotionPayment");
        request.put("Amount", amount);
        request.put("PartyA", yamlConfig.getShortcodePartyA());
        request.put("PartyB", phoneNumber);
        request.put("Remarks", "PromotionPayment");
        request.put("QueueTimeOutURL", queueTimeoutUrl);
        request.put("ResultURL", resultUrl);
        request.put("Occasion", "");
        
        return request.toString();
    }

    @Override
    public String getaccountBalance() {
        String queueTimeoutUrl = yamlConfig.getNgrok() + yamlConfig.getAccBalQueueTimeOutUrl();
        String resultUrl = yamlConfig.getNgrok() + yamlConfig.getAccBalResultUrl();
        
        JSONObject request = new JSONObject();
        request.put("Initiator", yamlConfig.getInitiator());
        request.put("SecurityCredential", yamlConfig.getSecurityCredential());
        request.put("CommandID", "AccountBalance");
        request.put("PartyA", yamlConfig.getShortcodePartyA());
        request.put("IdentifierType", yamlConfig.getSenderIdentifierOrganizationShortcode());
        request.put("Remarks", "accountBalance");
        request.put("QueueTimeOutURL", queueTimeoutUrl);
        request.put("ResultURL", resultUrl);
        
        return request.toString();
    }

    @Override
    public String transactionStatus(String phoneNumber, String transactionId) {
        String queueTimeoutUrl = yamlConfig.getNgrok() + yamlConfig.getTransactionStatusQueueTimeOutUrl();
        String resultUrl= yamlConfig.getNgrok() + yamlConfig.getTransactionStatusResultUrl();
        
        JSONObject request =new JSONObject();
        request.put("Initiator", yamlConfig.getInitiator());
        request.put("SecurityCredential", yamlConfig.getSecurityCredential());
        request.put("CommandID", "TransactionStatusQuery");
        request.put("TransactionID", transactionId);
        request.put("transactionId", phoneNumber);
        request.put("IdentifierType", 1);
        request.put("ResultURL", resultUrl);
        request.put("QueueTimeOutURL", queueTimeoutUrl);
        request.put("Remarks", "transaction status");
        request.put("Occasion", "transaction status");
        
        return request.toString();
    }   
    
}
    