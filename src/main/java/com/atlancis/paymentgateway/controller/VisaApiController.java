/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.atlancis.paymentgateway.controller;

import com.atlancis.paymentgateway.service.visaService.VisaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author nthemba
 */

@RestController
@RequestMapping("/visa/v1")
public class VisaApiController {

    @Autowired
    VisaService visaService;
    //Generate a key 
    @RequestMapping(value = "/generatekey", method = RequestMethod.POST, produces= MediaType.APPLICATION_JSON_VALUE)
    public void generateKey(){
        visaService.generateVisaKey();
    }
            
}
