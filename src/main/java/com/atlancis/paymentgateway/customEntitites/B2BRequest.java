/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.atlancis.paymentgateway.customEntitites;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 *
 * @author nthemba
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "shortCode",
    "amount",
    "accRef",
    "remarks",
    "resultUrl"
})

public class B2BRequest {

    @JsonProperty("shortCode")
    private String shortCode;
    @JsonProperty("amount")
    private Double amount;
    @JsonProperty("accRef")
    private String accRef;
    @JsonProperty("remarks")
    private String remarks;
    @JsonProperty("resultUrl")
    private String resultUrl;

    @JsonProperty("shortCode")
    public String getShortCode() {
        return shortCode;
    }

    @JsonProperty("shortCode")
    public void setShortCode(String shortCode) {
        this.shortCode = shortCode;
    }

    @JsonProperty("amount")
    public Double getAmount() {
        return amount;
    }

    @JsonProperty("amount")
    public void setAmount(Double amount) {
        this.amount = amount;
    }

    @JsonProperty("accRef")
    public String getAccRef() {
        return accRef;
    }

    @JsonProperty("accRef")
    public void setAccRef(String accRef) {
        this.accRef = accRef;
    }

    @JsonProperty("remarks")
    public String getRemarks() {
        return remarks;
    }

    @JsonProperty("remarks")
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    @JsonProperty("resultUrl")
    public String getResultUrl() {
        return resultUrl;
    }

    @JsonProperty("resultUrl")
    public void setResultUrl(String resultUrl) {
        this.resultUrl = resultUrl;
    }
}
