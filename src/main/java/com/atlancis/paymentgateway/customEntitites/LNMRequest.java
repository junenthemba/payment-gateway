/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.atlancis.paymentgateway.customEntitites;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 *
 * @author nthemba
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "phoneNumber",
    "amount",
    "accRef",
    "transactionDesc",
    "resultUrl"
})
public class LNMRequest {

    @JsonProperty("phoneNumber")
    private String phoneNumber;
    @JsonProperty("amount")
    private Double amount;
    @JsonProperty("accRef")
    private String accRef;
    @JsonProperty("transactionDesc")
    private String transactionDesc;
    @JsonProperty("resultUrl")
    private String resultUrl;

    @JsonProperty("phoneNumber")
    public String getPhoneNumber() {
        return phoneNumber;
    }

    @JsonProperty("phoneNumber")
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @JsonProperty("amount")
    public Double getAmount() {
        return amount;
    }

    @JsonProperty("amount")
    public void setAmount(Double amount) {
        this.amount = amount;
    }

    @JsonProperty("accRef")
    public String getAccRef() {
        return accRef;
    }

    @JsonProperty("accRef")
    public void setAccRef(String accRef) {
        this.accRef = accRef;
    }

    @JsonProperty("transactionDesc")
    public String getTransactionDesc() {
        return transactionDesc;
    }

    @JsonProperty("transactionDesc")
    public void setTransactionDesc(String transactionDesc) {
        this.transactionDesc = transactionDesc;
    }

    @JsonProperty("resultUrl")
    public String getResultUrl() {
        return resultUrl;
    }

    @JsonProperty("resultUrl")
    public void setResultUrl(String resultUrl) {
        this.resultUrl = resultUrl;
    }
}
