/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.atlancis.paymentgateway.messaging;

import com.atlancis.paymentgateway.configuration.MessagingConfig;
import com.atlancis.paymentgateway.service.mpesaService.MpesaTransactionType;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 *
 * @author nthemba
 */
@Service
public class MpesaSendMoneySender {
    
//    private Logger log =  Logger.getLogger(MpesaSendMoneySender.class.getName());
//    
//    private final RabbitTemplate rabbitTemplate;
//    
//    @Autowired
//    public MpesaSendMoneySender(final RabbitTemplate rabbitTemplate) {
//        this.rabbitTemplate = rabbitTemplate;
//    }
//    
////    @Scheduled(fixedDelay = 3000L)
//    public String sendMessage(MpesaTransactionType command,String source, Double amount) {
//        JSONObject params = new JSONObject();
//        params.put("command",command);
//        params.put("source", source);
//        params.put("amount", amount);
//       
//        final String message = params.toString();
//        log.info("Sending message... " + message);
//        
//        try {
//            rabbitTemplate.convertAndSend(MessagingConfig.EXCHANGE_NAME, MessagingConfig.ROUTING_KEY_LNM, message);
//        }catch(Exception e){
//            log.log(Level.SEVERE, "Failed to send Mpesa message");
//            e.printStackTrace();
//        }
//        
//        String response = "Request has been received and ready for processing";
//        
//        return response;
//    }
}
