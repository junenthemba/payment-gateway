/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.atlancis.paymentgateway.messaging;

import com.atlancis.paymentgateway.configuration.MessagingConfig;
import com.atlancis.paymentgateway.configuration.MpesaConfig;
import com.atlancis.paymentgateway.service.mpesaService.MpesaApiService;
import com.atlancis.paymentgateway.service.mpesaService.MpesaTransactionType;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.atlancis.paymentgateway.service.mpesaService.MpesaRequestService;

/**
 *
 * @author nthemba
 */
@Service
public class MpesaSendMoneyReceiver {
    
//    private Logger log =  Logger.getLogger(MpesaSendMoneyReceiver.class.getName());
//    
//    @Autowired
//    MpesaRequestService mpesaRequest;
//    
//    @Autowired
//    MpesaApiService mpesaService;
//    
//    @Autowired
//    MpesaConfig yamlConfig;
//    
//    @RabbitListener(queues = MessagingConfig.QUEUE_LNM)
//    public void receiveMessage(final Message message) {
//        log.info("Received message as generic: {}" + message.toString());
//        
//        String text = new String(message.getBody());
//        
//        log.info("Received message body " + text);
//        
//        try{
//          String res = sendRequest(text);
//          
//          log.log(Level.INFO, "Safaricom response " + res);
//        }catch(Exception e){
//            e.printStackTrace();
//        }
//    }
//    
//    public String sendRequest(String text){
//        
//        
//        JSONObject body = new JSONObject(text);
//        
//        String command = body.optString("command");
//        String source = body.optString("source");
//        Double amount = body.optDouble("amount");
//        
//        String sandboxUrl = null;
//        String response;
//        String requestBody = null;
//        
//        switch(command){
//            case "LNM_TRANS":
//                sandboxUrl = yamlConfig.getLnmSandBoxUrl();
//                
//                requestBody = mpesaRequest.lipaNaMpesaRequest(source,amount);
//                
//                break;
//            
//            case "C2B_TRANS":
//                sandboxUrl = yamlConfig.getC2bSimulateSandBoxUrl();
//                
//                requestBody = mpesaRequest.c2bSimulate(source, amount);
//                                
//                break;
//            
//            case "B2B_TRANS":
//                sandboxUrl = yamlConfig.getB2bSandBoxUrl();
//                
//                requestBody = mpesaRequest.b2bPaymentRequest(source, amount);
//                
//                break;
//                
//            case "B2C_TRANS":
//                sandboxUrl = yamlConfig.getB2cSandBoxUrl();
//                
//                requestBody = mpesaRequest.b2cPaymentRequest(source, amount);
//                                    
//            default:
//                response = "Invalid transaction";
//        }
//        
//        response = mpesaService.sendRequest(sandboxUrl, requestBody);
//        return response;
//    }
//    
}
