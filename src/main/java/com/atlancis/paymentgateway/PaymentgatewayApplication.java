package com.atlancis.paymentgateway;

import com.atlancis.paymentgateway.configuration.MpesaConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class PaymentgatewayApplication {
    @Autowired MpesaConfig myConfig;

	public static void main(String[] args) {
		SpringApplication.run(PaymentgatewayApplication.class, args);
	}
}
