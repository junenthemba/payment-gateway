/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.atlancis.paymentgateway.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 *
 * @author nthemba
 */
@Entity
@Table(name = "mpesa_Request")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updatedAt"}, 
        allowGetters = true)
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MpesaRequest.findAll", query = "SELECT m FROM MpesaRequest m"),
    @NamedQuery(name = "MpesaRequest.findById", query = "SELECT m FROM MpesaRequest m WHERE m.id = :id"),
    @NamedQuery(name = "MpesaRequest.findByIpAddress", query = "SELECT m FROM MpesaRequest m WHERE m.ipAddress = :ipAddress"),
    @NamedQuery(name = "MpesaRequest.findByRequestType", query = "SELECT m FROM MpesaRequest m WHERE m.requestType = :requestType"),
    @NamedQuery(name = "MpesaRequest.findByRequestStatus", query = "SELECT m FROM MpesaRequest m WHERE m.requestStatus = :requestStatus"),
    @NamedQuery(name = "MpesaRequest.findByCreatedAt", query = "SELECT m FROM MpesaRequest m WHERE m.createdAt = :createdAt"),
    @NamedQuery(name = "MpesaRequest.findByUpdatedAt", query = "SELECT m FROM MpesaRequest m WHERE m.updatedAt = :updatedAt")})
public class MpesaRequest implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "ipAddress")
    private String ipAddress;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 2147483647)
    @Column(name = "requestDetails")
    private String requestDetails;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "requestType")
    private String requestType;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "requestStatus")
    private String requestStatus;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "requestResult")
    private String requestResult;
    @Basic(optional = false)
    @NotNull
    @Column(name = "createdAt")
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    private Date createdAt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "updatedAt")
    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    private Date updatedAt;

    public MpesaRequest() {
    }

    public MpesaRequest(Long id) {
        this.id = id;
    }

    public MpesaRequest(Long id, String ipAddress, String requestDetails, String requestType, String requestStatus, Date createdAt, Date updatedAt) {
        this.id = id;
        this.ipAddress = ipAddress;
        this.requestDetails = requestDetails;
        this.requestType = requestType;
        this.requestStatus = requestStatus;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getRequestDetails() {
        return requestDetails;
    }

    public void setRequestDetails(String requestDetails) {
        this.requestDetails = requestDetails;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(String requestStatus) {
        this.requestStatus = requestStatus;
    }

    public String getRequestResult() {
        return requestResult;
    }

    public void setRequestResult(String requestResult) {
        this.requestResult = requestResult;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MpesaRequest)) {
            return false;
        }
        MpesaRequest other = (MpesaRequest) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.atlancis.paymentgateway.model.MpesaRequest[ id=" + id + " ]";
    }
    
}
