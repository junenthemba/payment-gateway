/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.atlancis.paymentgateway.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 *
 * @author nthemba
 */
@Entity
@Table(name = "lnm_mpesa_transaction")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updatedAt"}, 
        allowGetters = true)
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LnmMpesaTransaction.findAll", query = "SELECT l FROM LnmMpesaTransaction l"),
    @NamedQuery(name = "LnmMpesaTransaction.findById", query = "SELECT l FROM LnmMpesaTransaction l WHERE l.id = :id"),
    @NamedQuery(name = "LnmMpesaTransaction.findByIpAddress", query = "SELECT l FROM LnmMpesaTransaction l WHERE l.ipAddress = :ipAddress"),
    @NamedQuery(name = "LnmMpesaTransaction.findByMerchantRequestID", query = "SELECT l FROM LnmMpesaTransaction l WHERE l.merchantRequestID = :merchantRequestID"),
    @NamedQuery(name = "LnmMpesaTransaction.findByCheckoutRequestID", query = "SELECT l FROM LnmMpesaTransaction l WHERE l.checkoutRequestID = :checkoutRequestID"),
    @NamedQuery(name = "LnmMpesaTransaction.findByResultCode", query = "SELECT l FROM LnmMpesaTransaction l WHERE l.resultCode = :resultCode"),
    @NamedQuery(name = "LnmMpesaTransaction.findByResultDesc", query = "SELECT l FROM LnmMpesaTransaction l WHERE l.resultDesc = :resultDesc"),
    @NamedQuery(name = "LnmMpesaTransaction.findByPhoneNumber", query = "SELECT l FROM LnmMpesaTransaction l WHERE l.phoneNumber = :phoneNumber"),
    @NamedQuery(name = "LnmMpesaTransaction.findByAmount", query = "SELECT l FROM LnmMpesaTransaction l WHERE l.amount = :amount"),
    @NamedQuery(name = "LnmMpesaTransaction.findByMpesaReceiptNumber", query = "SELECT l FROM LnmMpesaTransaction l WHERE l.mpesaReceiptNumber = :mpesaReceiptNumber"),
    @NamedQuery(name = "LnmMpesaTransaction.findByBalance", query = "SELECT l FROM LnmMpesaTransaction l WHERE l.balance = :balance"),
    @NamedQuery(name = "LnmMpesaTransaction.findByTransactionDate", query = "SELECT l FROM LnmMpesaTransaction l WHERE l.transactionDate = :transactionDate"),
    @NamedQuery(name = "LnmMpesaTransaction.findByCreatedAt", query = "SELECT l FROM LnmMpesaTransaction l WHERE l.createdAt = :createdAt"),
    @NamedQuery(name = "LnmMpesaTransaction.findByUpdatedAt", query = "SELECT l FROM LnmMpesaTransaction l WHERE l.updatedAt = :updatedAt")})
public class LnmMpesaTransaction implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "ipAddress")
    private String ipAddress;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "merchantRequestID")
    private String merchantRequestID;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "checkoutRequestID")
    private String checkoutRequestID;
    @Basic(optional = false)
    @NotNull
    @Column(name = "resultCode")
    private int resultCode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "resultDesc")
    private String resultDesc;
    @Size(max = 100)
    @Column(name = "phoneNumber")
    private String phoneNumber;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "amount")
    private Double amount;
    @Size(max = 100)
    @Column(name = "mpesaReceiptNumber")
    private String mpesaReceiptNumber;
    @Column(name = "balance")
    private Double balance;
    @Column(name = "transactionDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date transactionDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "createdAt")
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    private Date createdAt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "updatedAt")
    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    private Date updatedAt;

    public LnmMpesaTransaction() {
    }

    public LnmMpesaTransaction(Long id) {
        this.id = id;
    }

    public LnmMpesaTransaction(Long id, String ipAddress, String merchantRequestID, String checkoutRequestID, int resultCode, String resultDesc, Date createdAt, Date updatedAt) {
        this.id = id;
        this.ipAddress = ipAddress;
        this.merchantRequestID = merchantRequestID;
        this.checkoutRequestID = checkoutRequestID;
        this.resultCode = resultCode;
        this.resultDesc = resultDesc;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getMerchantRequestID() {
        return merchantRequestID;
    }

    public void setMerchantRequestID(String merchantRequestID) {
        this.merchantRequestID = merchantRequestID;
    }

    public String getCheckoutRequestID() {
        return checkoutRequestID;
    }

    public void setCheckoutRequestID(String checkoutRequestID) {
        this.checkoutRequestID = checkoutRequestID;
    }

    public int getResultCode() {
        return resultCode;
    }

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }

    public String getResultDesc() {
        return resultDesc;
    }

    public void setResultDesc(String resultDesc) {
        this.resultDesc = resultDesc;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getMpesaReceiptNumber() {
        return mpesaReceiptNumber;
    }

    public void setMpesaReceiptNumber(String mpesaReceiptNumber) {
        this.mpesaReceiptNumber = mpesaReceiptNumber;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LnmMpesaTransaction)) {
            return false;
        }
        LnmMpesaTransaction other = (LnmMpesaTransaction) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.atlancis.paymentgateway.model.LnmMpesaTransaction[ id=" + id + " ]";
    }
    
}
