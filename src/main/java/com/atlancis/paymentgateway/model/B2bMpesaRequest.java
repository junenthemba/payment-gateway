/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.atlancis.paymentgateway.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 *
 * @author nthemba
 */
@Entity
@Table(name = "b2b_mpesa_request")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updatedAt"}, 
        allowGetters = true)
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "B2bMpesaRequest.findAll", query = "SELECT b FROM B2bMpesaRequest b"),
    @NamedQuery(name = "B2bMpesaRequest.findById", query = "SELECT b FROM B2bMpesaRequest b WHERE b.id = :id"),
    @NamedQuery(name = "B2bMpesaRequest.findByIpAddress", query = "SELECT b FROM B2bMpesaRequest b WHERE b.ipAddress = :ipAddress"),
    @NamedQuery(name = "B2bMpesaRequest.findByShortCode", query = "SELECT b FROM B2bMpesaRequest b WHERE b.shortCode = :shortCode"),
    @NamedQuery(name = "B2bMpesaRequest.findByAmount", query = "SELECT b FROM B2bMpesaRequest b WHERE b.amount = :amount"),
    @NamedQuery(name = "B2bMpesaRequest.findByAccountRef", query = "SELECT b FROM B2bMpesaRequest b WHERE b.accountRef = :accountRef"),
    @NamedQuery(name = "B2bMpesaRequest.findByRemarks", query = "SELECT b FROM B2bMpesaRequest b WHERE b.remarks = :remarks"),
    @NamedQuery(name = "B2bMpesaRequest.findByResultUrl", query = "SELECT b FROM B2bMpesaRequest b WHERE b.resultUrl = :resultUrl"),
    @NamedQuery(name = "B2bMpesaRequest.findByOriginatorConverstionID", query = "SELECT b FROM B2bMpesaRequest b WHERE b.originatorConverstionID = :originatorConverstionID"),
    @NamedQuery(name = "B2bMpesaRequest.findByConversationID", query = "SELECT b FROM B2bMpesaRequest b WHERE b.conversationID = :conversationID"),
    @NamedQuery(name = "B2bMpesaRequest.findByResponseDescription", query = "SELECT b FROM B2bMpesaRequest b WHERE b.responseDescription = :responseDescription"),
    @NamedQuery(name = "B2bMpesaRequest.findByCreatedAt", query = "SELECT b FROM B2bMpesaRequest b WHERE b.createdAt = :createdAt"),
    @NamedQuery(name = "B2bMpesaRequest.findByUpdatedAt", query = "SELECT b FROM B2bMpesaRequest b WHERE b.updatedAt = :updatedAt")})
public class B2bMpesaRequest implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "ipAddress")
    private String ipAddress;
    @Size(max = 200)
    @Column(name = "shortCode")
    private String shortCode;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "amount")
    private Double amount;
    @Size(max = 200)
    @Column(name = "accountRef")
    private String accountRef;
    @Size(max = 200)
    @Column(name = "remarks")
    private String remarks;
    @Size(max = 200)
    @Column(name = "resultUrl")
    private String resultUrl;
    @Size(max = 200)
    @Column(name = "OriginatorConverstionID")
    private String originatorConverstionID;
    @Size(max = 200)
    @Column(name = "ConversationID")
    private String conversationID;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "ResponseDescription")
    private String responseDescription;
    @Basic(optional = false)
    @NotNull
    @Column(name = "createdAt")
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    private Date createdAt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "updatedAt")
    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    private Date updatedAt;

    public B2bMpesaRequest() {
    }

    public B2bMpesaRequest(Long id) {
        this.id = id;
    }

    public B2bMpesaRequest(Long id, String ipAddress, String originatorConverstionID, String conversationID, String responseDescription, Date createdAt, Date updatedAt) {
        this.id = id;
        this.ipAddress = ipAddress;
        this.originatorConverstionID = originatorConverstionID;
        this.conversationID = conversationID;
        this.responseDescription = responseDescription;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getShortCode() {
        return shortCode;
    }

    public void setShortCode(String shortCode) {
        this.shortCode = shortCode;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getAccountRef() {
        return accountRef;
    }

    public void setAccountRef(String accountRef) {
        this.accountRef = accountRef;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getResultUrl() {
        return resultUrl;
    }

    public void setResultUrl(String resultUrl) {
        this.resultUrl = resultUrl;
    }

    public String getOriginatorConverstionID() {
        return originatorConverstionID;
    }

    public void setOriginatorConverstionID(String originatorConverstionID) {
        this.originatorConverstionID = originatorConverstionID;
    }

    public String getConversationID() {
        return conversationID;
    }

    public void setConversationID(String conversationID) {
        this.conversationID = conversationID;
    }

    public String getResponseDescription() {
        return responseDescription;
    }

    public void setResponseDescription(String responseDescription) {
        this.responseDescription = responseDescription;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof B2bMpesaRequest)) {
            return false;
        }
        B2bMpesaRequest other = (B2bMpesaRequest) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.atlancis.paymentgateway.model.B2bMpesaRequest[ id=" + id + " ]";
    }
    
}
