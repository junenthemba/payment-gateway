/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.atlancis.paymentgateway.repositories;

import com.atlancis.paymentgateway.model.B2bTransaction;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author nthemba
 */
@Repository
public interface B2bTransactionRepository extends JpaRepository<B2bTransaction, Long> {
    
    @Query("SELECT b FROM B2bTransaction b WHERE b.transactionID = :transactionID")
    B2bTransaction findByTransactionID(@Param("transactionID") String transactionID);
}
