/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.atlancis.paymentgateway.repositories;

import com.atlancis.paymentgateway.model.LnmMpesaRequest;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author nthemba
 */
@Repository
public interface LnmMpesaRequestRepository extends JpaRepository<LnmMpesaRequest, Long>{
    
    @Query("SELECT l FROM LnmMpesaRequest l where l.accountRef = :accountRef") 
    List<LnmMpesaRequest> findByAccountRef(@Param("accountRef") String accountRef);
    
    @Query("SELECT l.resultUrl FROM LnmMpesaRequest l where l.merchantRequestID = :merchantRequestId AND l.checkoutRequestID = :checkoutRequestId")
    String findResultUrlByMerchantandCheckoutId(@Param("merchantRequestId") String merchantRequestId, @Param("checkoutRequestId") String checkoutRequestId);
    
    @Query("SELECT l FROM LnmMpesaRequest l where l.merchantRequestID = :merchantRequestId AND l.checkoutRequestID = :checkoutRequestId")
    LnmMpesaRequest findByMerchantandCheckoutId(@Param("merchantRequestId") String merchantRequestId, @Param("checkoutRequestId") String checkoutRequestId);
}
