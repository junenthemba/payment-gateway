/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.atlancis.paymentgateway.repositories;

import com.atlancis.paymentgateway.model.LnmMpesaTransaction;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author nthemba
 */
@Repository
public interface LnmMpesaTransactionRepository extends JpaRepository<LnmMpesaTransaction, Long>{
    
    @Query("SELECT l FROM LnmMpesaTransaction l WHERE l.mpesaReceiptNumber = :TransID")
    LnmMpesaTransaction findByMpesaReceiptNumber(@Param("TransID") String mpesaReceiptNumber);
    
    List<LnmMpesaTransaction> findByPhoneNumber(String phoneNumber);
}
