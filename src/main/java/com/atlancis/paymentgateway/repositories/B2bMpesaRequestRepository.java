/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.atlancis.paymentgateway.repositories;

import com.atlancis.paymentgateway.model.B2bMpesaRequest;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author nthemba
 */
@Repository
public interface B2bMpesaRequestRepository extends JpaRepository<B2bMpesaRequest, Long>{
    
    @Query("SELECT b FROM B2bMpesaRequest b where b.accountRef = :accountRef") 
    List<B2bMpesaRequest> findByAccountRef(@Param("accountRef") String accountRef);
    
    @Query("SELECT b FROM B2bMpesaRequest b where b.originatorConverstionID = :originatorConverstionID AND b.conversationID = :conversationID")
    B2bMpesaRequest findByOriginatorConversationAndConversationID(@Param("originatorConverstionID") String originatorConverstionID, @Param("conversationID") String conversationID);
}
