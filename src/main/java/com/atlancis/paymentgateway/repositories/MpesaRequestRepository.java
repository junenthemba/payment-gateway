/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.atlancis.paymentgateway.repositories;

import com.atlancis.paymentgateway.model.MpesaRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.atlancis.paymentgateway.service.mpesaService.MpesaRequestService;

/**
 *
 * @author nthemba
 */
@Repository
public interface MpesaRequestRepository extends JpaRepository<MpesaRequest, Long> {
    
}
