-- Adminer 4.6.2 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP DATABASE IF EXISTS `payment-gateway`;
CREATE DATABASE `payment-gateway` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `payment-gateway`;

DROP TABLE IF EXISTS `b2b_mpesa_request`;
CREATE TABLE `b2b_mpesa_request` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ipAddress` varchar(200) NOT NULL,
  `shortCode` varchar(200) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `accountRef` varchar(200) DEFAULT NULL,
  `remarks` varchar(200) DEFAULT NULL,
  `resultUrl` varchar(200) DEFAULT NULL,
  `OriginatorConverstionID` varchar(200) DEFAULT NULL,
  `ConversationID` varchar(200) DEFAULT NULL,
  `ResponseDescription` varchar(200) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `b2b_mpesa_request` (`id`, `ipAddress`, `shortCode`, `amount`, `accountRef`, `remarks`, `resultUrl`, `OriginatorConverstionID`, `ConversationID`, `ResponseDescription`, `createdAt`, `updatedAt`) VALUES
(1,	'0:0:0:0:0:0:0:1',	'600000',	10,	'CBL0007',	'Payment of services',	'http://localhost:8081/paymentgateway/mpesaapi/v1/billing',	'25794-3459234-2',	'AG_20180316_000047f786e1e2787194',	'Accept the service request successfully.',	'2018-03-16 10:13:59',	'2018-03-16 10:13:59'),
(2,	'0:0:0:0:0:0:0:1',	'600000',	10,	'CBL0007dup',	'Payment of services',	'http://localhost:8081/paymentgateway/mpesaapi/v1/billing',	'',	'',	'Account reference number already exists',	'2018-03-16 11:13:44',	'2018-03-16 11:13:44'),
(3,	'0:0:0:0:0:0:0:1',	'600000',	NULL,	'CBL0007',	'Payment of services',	'http://localhost:8081/paymentgateway/mpesaapi/v1/billing',	'',	'',	'Some parameter is missing',	'2018-03-16 11:14:27',	'2018-03-16 11:14:27'),
(4,	'0:0:0:0:0:0:0:1',	'600000',	10,	'CBL0007dup',	'Payment of services',	'http://localhost:8081/paymentgateway/mpesaapi/v1/billing',	'',	'',	'Account reference number already exists',	'2018-03-16 11:14:47',	'2018-03-16 11:14:47'),
(5,	'0:0:0:0:0:0:0:1',	'600000',	10,	'CBL0008',	'Payment of services',	'http://localhost:8081/paymentgateway/mpesaapi/v1/billing',	'17378-166287-2',	'AG_20180320_000069d6daa2e9586c1f',	'Accept the service request successfully.',	'2018-03-20 15:26:46',	'2018-03-20 15:26:46'),
(6,	'0:0:0:0:0:0:0:1',	'60000',	10,	'CBL0009',	'Payment of services',	'http://localhost:8081/paymentgateway/mpesaapi/v1/billing',	'17379-307727-2',	'AG_20180321_000043e802bb5746811c',	'Accept the service request successfully.',	'2018-03-21 10:23:08',	'2018-03-21 10:23:08'),
(7,	'0:0:0:0:0:0:0:1',	'60000',	10,	'CBL0010',	'Payment of services',	'http://localhost:8081/paymentgateway/mpesaapi/v1/billing',	'17377-307913-2',	'AG_20180321_000051c073c05d31382a',	'Accept the service request successfully.',	'2018-03-21 10:24:20',	'2018-03-21 10:24:20'),
(8,	'0:0:0:0:0:0:0:1',	'60000',	10,	'CBL0011',	'Payment of services',	'http://localhost:8081/paymentgateway/mpesaapi/v1/billing',	'16685-4549446-2',	'AG_20180321_00004ea054086f087a37',	'Accept the service request successfully.',	'2018-03-21 10:44:54',	'2018-03-21 10:44:54'),
(9,	'0:0:0:0:0:0:0:1',	'600000',	10,	'CBL0012',	'Payment of services',	'http://localhost:8082/paymentgateway/mpesa/res/b2b',	'17377-320760-2',	'AG_20180321_00007c81b26787023d33',	'Accept the service request successfully.',	'2018-03-21 11:48:48',	'2018-03-21 11:48:48'),
(10,	'0:0:0:0:0:0:0:1',	'600000',	10,	'CBL0014',	'Payment of services',	'http://localhost:8081/paymentgateway/mpesa/res/b2b',	'12362-1592650-2',	'AG_20180327_00006fe43f44fc62f6ad',	'Accept the service request successfully.',	'2018-03-27 10:27:59',	'2018-03-27 10:27:59'),
(11,	'0:0:0:0:0:0:0:1',	'600000',	10,	'CBL0015',	'Payment of services',	'http://localhost:8081/paymentgateway/mpesa/res/b2b',	'12363-1594113-2',	'AG_20180327_0000464727b0c65c5f73',	'Accept the service request successfully.',	'2018-03-27 10:34:11',	'2018-03-27 10:34:11'),
(12,	'0:0:0:0:0:0:0:1',	'600000',	10,	'CBL0016',	'Payment of services',	'http://localhost:8081/paymentgateway/mpesa/res/b2b',	'20733-1947058-2',	'AG_20180328_000041b92ec20f80bf40',	'Accept the service request successfully.',	'2018-03-28 09:15:19',	'2018-03-28 09:15:19'),
(13,	'0:0:0:0:0:0:0:1',	'600000',	10,	'CBL0017',	'Payment of services',	'http://localhost:8081/paymentgateway/mpesa/res/b2b',	'12365-2158684-2',	'AG_20180328_000067bb82b3edaa47bd',	'Accept the service request successfully.',	'2018-03-28 15:46:29',	'2018-03-28 15:46:29');

DROP TABLE IF EXISTS `b2b_transaction`;
CREATE TABLE `b2b_transaction` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ipAddress` varchar(200) NOT NULL,
  `resultType` int(11) NOT NULL,
  `resultCode` int(11) NOT NULL,
  `resultDesc` varchar(200) NOT NULL,
  `originatorConversationID` varchar(200) NOT NULL,
  `conversationID` varchar(200) NOT NULL,
  `transactionID` varchar(200) NOT NULL,
  `debitAccountBalance` varchar(200) DEFAULT NULL,
  `initiatorAccountCurrentBalance` varchar(200) DEFAULT NULL,
  `debitAccountCurrentBalance` varchar(200) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `transCompletedTime` datetime DEFAULT NULL,
  `debitPartyCharges` varchar(200) DEFAULT NULL,
  `creditPartyPublicName` varchar(200) DEFAULT NULL,
  `debitPartyPublicName` varchar(200) DEFAULT NULL,
  `creditAccountBalance` varchar(200) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `transactionIDUnique` (`transactionID`),
  UNIQUE KEY `originatorConversationIDUnique` (`originatorConversationID`),
  UNIQUE KEY `conversationIDUnique` (`conversationID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `b2b_transaction` (`id`, `ipAddress`, `resultType`, `resultCode`, `resultDesc`, `originatorConversationID`, `conversationID`, `transactionID`, `debitAccountBalance`, `initiatorAccountCurrentBalance`, `debitAccountCurrentBalance`, `amount`, `transCompletedTime`, `debitPartyCharges`, `creditPartyPublicName`, `debitPartyPublicName`, `creditAccountBalance`, `createdAt`, `updatedAt`) VALUES
(1,	'',	0,	0,	'The service request is processed successfully.',	'16680-740163-2',	'AG_20180302_0000486bfef45a93e5ab',	'MC291H471J',	'Working Account|KES|32210.00|32210.00|0.00|0.00',	'{Amount={BasicAmount=32210.00,MinimumAmount=3221000,CurrencyCode=KES}}',	'{Amount={BasicAmount=32210.00,MinimumAmount=3221000,CurrencyCode=KES}}',	10,	'2018-03-02 13:32:58',	'Business Transfer Charge|KES|0.00&Business Transfer Charge by Receiver|KES|0.00',	'600000 - saf test org',	'600602 - Safaricom625',	'Working Account|KES|2501990.00|2501990.00|1056.00|0.00&Charges Paid Account|KES|-5335.00|-5445.00|0.00|110.00',	'2018-03-02 13:33:02',	'2018-03-02 13:33:02'),
(2,	'',	0,	0,	'The service request is processed successfully.',	'13602-824583-2',	'AG_20180314_000046e6b94d053361cc',	'MCE51H49G3',	'Working Account|KES|31150.00|31150.00|0.00|0.00',	'{Amount={BasicAmount=31150.00,MinimumAmount=3115000,CurrencyCode=KES}}',	'{Amount={BasicAmount=31150.00,MinimumAmount=3115000,CurrencyCode=KES}}',	10,	'2018-03-14 11:22:06',	'Business Transfer Charge|KES|0.00&Business Transfer Charge by Receiver|KES|0.00',	'600000 - saf test org',	'600602 - Safaricom625',	'Working Account|KES|2503100.00|2503100.00|1056.00|0.00&Charges Paid Account|KES|-6765.00|-6875.00|0.00|110.00',	'2018-03-14 11:22:09',	'2018-03-14 11:22:09'),
(3,	'127.0.0.1',	0,	0,	'The service request is processed successfully.',	'13597-983992-2',	'AG_20180315_0000724d63e1603f3380',	'MCF21H49QK',	'Working Account|KES|31130.00|31130.00|0.00|0.00',	'{Amount={BasicAmount=31130.00,MinimumAmount=3113000,CurrencyCode=KES}}',	'{Amount={BasicAmount=31130.00,MinimumAmount=3113000,CurrencyCode=KES}}',	10,	'2018-03-15 11:19:59',	'Business Transfer Charge|KES|0.00&Business Transfer Charge by Receiver|KES|0.00',	'600000 - saf test org',	'600602 - Safaricom625',	'Working Account|KES|2506120.00|2506120.00|1056.00|0.00&Charges Paid Account|KES|-6765.00|-6875.00|0.00|110.00',	'2018-03-15 11:20:01',	'2018-03-15 11:20:01'),
(4,	'127.0.0.1',	0,	7,	'The ReceiverParty information is invalid.',	'13588-984938-2',	'AG_20180315_000056705278ac276be3',	'MCF0000000',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	'2018-03-15 11:27:35',	'2018-03-15 11:27:35'),
(5,	'127.0.0.1',	0,	0,	'The service request is processed successfully.',	'25796-3305690-2',	'AG_20180315_00006712d0970b286aba',	'MCF81H49XO',	'Working Account|KES|31120.00|31120.00|0.00|0.00',	'{Amount={BasicAmount=31120.00,MinimumAmount=3112000,CurrencyCode=KES}}',	'{Amount={BasicAmount=31120.00,MinimumAmount=3112000,CurrencyCode=KES}}',	10,	'2018-03-15 16:47:58',	'Business Transfer Charge|KES|0.00&Business Transfer Charge by Receiver|KES|0.00',	'600000 - saf test org',	'600602 - Safaricom625',	'Working Account|KES|2506130.00|2506130.00|1056.00|0.00&Charges Paid Account|KES|-6765.00|-6875.00|0.00|110.00',	'2018-03-15 16:48:01',	'2018-03-15 16:48:01'),
(6,	'127.0.0.1',	0,	0,	'The service request is processed successfully.',	'16683-3318982-2',	'AG_20180315_00007b151975e27653c2',	'MCF01H49XQ',	'Working Account|KES|31110.00|31110.00|0.00|0.00',	'{Amount={BasicAmount=31110.00,MinimumAmount=3111000,CurrencyCode=KES}}',	'{Amount={BasicAmount=31110.00,MinimumAmount=3111000,CurrencyCode=KES}}',	10,	'2018-03-15 16:55:39',	'Business Transfer Charge|KES|0.00&Business Transfer Charge by Receiver|KES|0.00',	'600000 - saf test org',	'600602 - Safaricom625',	'Working Account|KES|2506140.00|2506140.00|1056.00|0.00&Charges Paid Account|KES|-6765.00|-6875.00|0.00|110.00',	'2018-03-15 16:55:41',	'2018-03-15 16:55:41'),
(7,	'127.0.0.1',	0,	0,	'The service request is processed successfully.',	'16678-3319651-2',	'AG_20180315_0000787cc0cd1fefff06',	'MCF31H49XT',	'Working Account|KES|31100.00|31100.00|0.00|0.00',	'{Amount={BasicAmount=31100.00,MinimumAmount=3110000,CurrencyCode=KES}}',	'{Amount={BasicAmount=31100.00,MinimumAmount=3110000,CurrencyCode=KES}}',	10,	'2018-03-15 16:59:34',	'Business Transfer Charge|KES|0.00&Business Transfer Charge by Receiver|KES|0.00',	'600000 - saf test org',	'600602 - Safaricom625',	'Working Account|KES|2506150.00|2506150.00|1056.00|0.00&Charges Paid Account|KES|-6765.00|-6875.00|0.00|110.00',	'2018-03-15 16:59:36',	'2018-03-15 16:59:36'),
(8,	'127.0.0.1',	0,	0,	'The service request is processed successfully.',	'16684-3467854-2',	'AG_20180316_000055070804ad10bc3c',	'MCG91H49ZX',	'Working Account|KES|31090.00|31090.00|0.00|0.00',	'{Amount={BasicAmount=31090.00,MinimumAmount=3109000,CurrencyCode=KES}}',	'{Amount={BasicAmount=31090.00,MinimumAmount=3109000,CurrencyCode=KES}}',	10,	'2018-03-16 09:29:38',	'Business Transfer Charge|KES|0.00&Business Transfer Charge by Receiver|KES|0.00',	'600000 - saf test org',	'600602 - Safaricom625',	'Working Account|KES|2506160.00|2506160.00|1056.00|0.00&Charges Paid Account|KES|-6765.00|-6875.00|0.00|110.00',	'2018-03-16 09:29:41',	'2018-03-16 09:29:41'),
(9,	'127.0.0.1',	0,	0,	'The service request is processed successfully.',	'16683-3471317-2',	'AG_20180316_000075dcffd8298526d7',	'MCG01H4A08',	'Working Account|KES|31080.00|31080.00|0.00|0.00',	'{Amount={BasicAmount=31080.00,MinimumAmount=3108000,CurrencyCode=KES}}',	'{Amount={BasicAmount=31080.00,MinimumAmount=3108000,CurrencyCode=KES}}',	10,	'2018-03-16 09:50:16',	'Business Transfer Charge|KES|0.00&Business Transfer Charge by Receiver|KES|0.00',	'600000 - saf test org',	'600602 - Safaricom625',	'Working Account|KES|2506170.00|2506170.00|1056.00|0.00&Charges Paid Account|KES|-6765.00|-6875.00|0.00|110.00',	'2018-03-16 09:50:18',	'2018-03-16 09:50:18'),
(10,	'127.0.0.1',	0,	0,	'The service request is processed successfully.',	'16679-3471546-2',	'AG_20180316_000069a6cf7b308e8bde',	'MCG21H4A0A',	'Working Account|KES|31070.00|31070.00|0.00|0.00',	'{Amount={BasicAmount=31070.00,MinimumAmount=3107000,CurrencyCode=KES}}',	'{Amount={BasicAmount=31070.00,MinimumAmount=3107000,CurrencyCode=KES}}',	10,	'2018-03-16 09:51:47',	'Business Transfer Charge|KES|0.00&Business Transfer Charge by Receiver|KES|0.00',	'600000 - saf test org',	'600602 - Safaricom625',	'Working Account|KES|2506180.00|2506180.00|1056.00|0.00&Charges Paid Account|KES|-6765.00|-6875.00|0.00|110.00',	'2018-03-16 09:51:49',	'2018-03-16 09:51:49'),
(11,	'127.0.0.1',	0,	0,	'The service request is processed successfully.',	'25794-3456613-2',	'AG_20180316_00004cb8939d2614aab5',	'MCG71H4A0F',	'Working Account|KES|31060.00|31060.00|0.00|0.00',	'{Amount={BasicAmount=31060.00,MinimumAmount=3106000,CurrencyCode=KES}}',	'{Amount={BasicAmount=31060.00,MinimumAmount=3106000,CurrencyCode=KES}}',	10,	'2018-03-16 09:58:15',	'Business Transfer Charge|KES|0.00&Business Transfer Charge by Receiver|KES|0.00',	'600000 - saf test org',	'600602 - Safaricom625',	'Working Account|KES|2506190.00|2506190.00|1056.00|0.00&Charges Paid Account|KES|-6765.00|-6875.00|0.00|110.00',	'2018-03-16 09:58:17',	'2018-03-16 09:58:17'),
(12,	'127.0.0.1',	0,	0,	'The service request is processed successfully.',	'25794-3457697-2',	'AG_20180316_000070a05c4993bb9a85',	'MCG81H4A0G',	'Working Account|KES|31050.00|31050.00|0.00|0.00',	'{Amount={BasicAmount=31050.00,MinimumAmount=3105000,CurrencyCode=KES}}',	'{Amount={BasicAmount=31050.00,MinimumAmount=3105000,CurrencyCode=KES}}',	10,	'2018-03-16 10:04:32',	'Business Transfer Charge|KES|0.00&Business Transfer Charge by Receiver|KES|0.00',	'600000 - saf test org',	'600602 - Safaricom625',	'Working Account|KES|2506200.00|2506200.00|1056.00|0.00&Charges Paid Account|KES|-6765.00|-6875.00|0.00|110.00',	'2018-03-16 10:04:34',	'2018-03-16 10:04:34'),
(13,	'127.0.0.1',	0,	0,	'The service request is processed successfully.',	'25794-3459234-2',	'AG_20180316_000047f786e1e2787194',	'MCG31H4A0L',	'Working Account|KES|31040.00|31040.00|0.00|0.00',	'{Amount={BasicAmount=31040.00,MinimumAmount=3104000,CurrencyCode=KES}}',	'{Amount={BasicAmount=31040.00,MinimumAmount=3104000,CurrencyCode=KES}}',	10,	'2018-03-16 10:13:57',	'Business Transfer Charge|KES|0.00&Business Transfer Charge by Receiver|KES|0.00',	'600000 - saf test org',	'600602 - Safaricom625',	'Working Account|KES|2506210.00|2506210.00|1056.00|0.00&Charges Paid Account|KES|-6765.00|-6875.00|0.00|110.00',	'2018-03-16 10:13:59',	'2018-03-16 10:13:59'),
(14,	'127.0.0.1',	0,	0,	'The service request is processed successfully.',	'17378-166287-2',	'AG_20180320_000069d6daa2e9586c1f',	'MCK81H4AQK',	'Working Account|KES|30130.00|30130.00|0.00|0.00',	'{Amount={BasicAmount=30130.00,MinimumAmount=3013000,CurrencyCode=KES}}',	'{Amount={BasicAmount=30130.00,MinimumAmount=3013000,CurrencyCode=KES}}',	10,	'2018-03-20 15:26:46',	'Business Transfer Charge|KES|0.00&Business Transfer Charge by Receiver|KES|0.00',	'600000 - saf test org',	'600602 - Safaricom625',	'Working Account|KES|2513020.00|2513020.00|1056.00|0.00&Charges Paid Account|KES|-7260.00|-7370.00|0.00|110.00',	'2018-03-20 15:26:48',	'2018-03-20 15:26:48'),
(15,	'127.0.0.1',	0,	7,	'The ReceiverParty information is invalid.',	'17379-307727-2',	'AG_20180321_000043e802bb5746811c',	'MCL0000000',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	'2018-03-21 10:23:09',	'2018-03-21 10:23:09'),
(20,	'127.0.0.1',	0,	0,	'The service request is processed successfully.',	'17377-320760-2',	'AG_20180321_00007c81b26787023d33',	'MCL61H4AUE',	'Working Account|KES|29120.00|29120.00|0.00|0.00',	'{Amount={BasicAmount=29120.00,MinimumAmount=2912000,CurrencyCode=KES}}',	'{Amount={BasicAmount=29120.00,MinimumAmount=2912000,CurrencyCode=KES}}',	10,	'2018-03-21 11:48:47',	'Business Transfer Charge|KES|0.00&Business Transfer Charge by Receiver|KES|0.00',	'600000 - saf test org',	'600602 - Safaricom625',	'Working Account|KES|2514030.00|2514030.00|1056.00|0.00&Charges Paid Account|KES|-7260.00|-7370.00|0.00|110.00',	'2018-03-21 11:48:49',	'2018-03-21 11:48:49');

DROP TABLE IF EXISTS `c2b_transaction`;
CREATE TABLE `c2b_transaction` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ipAddress` varchar(200) NOT NULL,
  `TransactionType` varchar(100) NOT NULL,
  `TransID` varchar(100) NOT NULL,
  `FirstName` varchar(100) NOT NULL,
  `MiddleName` varchar(100) NOT NULL,
  `LastName` varchar(100) NOT NULL,
  `MSISDN` varchar(100) NOT NULL,
  `TransAmount` double NOT NULL,
  `TransTime` datetime NOT NULL,
  `OrgAccountBalance` double NOT NULL,
  `BusinessShortCode` varchar(30) NOT NULL,
  `BillRefNumber` varchar(100) NOT NULL,
  `InvoiceNumber` varchar(100) DEFAULT NULL,
  `ThirdPartyTransID` varchar(100) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `transID` (`TransID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `c2b_transaction` (`id`, `ipAddress`, `TransactionType`, `TransID`, `FirstName`, `MiddleName`, `LastName`, `MSISDN`, `TransAmount`, `TransTime`, `OrgAccountBalance`, `BusinessShortCode`, `BillRefNumber`, `InvoiceNumber`, `ThirdPartyTransID`, `createdAt`, `updatedAt`) VALUES
(1,	'',	'Pay Bill',	'MBR01H46N4',	'John',	'J.',	'Doe',	'254708374149',	1,	'2609-06-27 13:38:25',	50448,	'600366',	'254708374149',	'',	'',	'2018-02-27 11:01:04',	'2018-02-27 11:01:04'),
(2,	'',	'Pay Bill',	'MBR01H46NE',	'John',	'J.',	'Doe',	'254708374149',	1,	'2609-06-27 13:38:35',	50454,	'600366',	'254708374149',	'',	'',	'2018-02-27 12:01:50',	'2018-02-27 12:01:50'),
(3,	'',	'Pay Bill',	'MBR11H46NF',	'John',	'J.',	'Doe',	'254708374149',	1,	'2609-06-27 13:38:35',	50455,	'600366',	'254708374149',	'',	'',	'2018-02-27 12:03:43',	'2018-02-27 12:03:43'),
(4,	'',	'Pay Bill',	'MBS01H46U2',	'John',	'J.',	'Doe',	'254708374149',	1,	'2609-06-27 13:54:44',	50457,	'600366',	'254708374149',	'',	'',	'2018-02-28 08:56:29',	'2018-02-28 08:56:29'),
(5,	'',	'Pay Bill',	'MCF51H49QN',	'John',	'J.',	'Doe',	'254708374149',	1,	'2609-06-28 14:05:13',	50458,	'600366',	'254708374149',	'',	'',	'2018-03-15 11:33:56',	'2018-03-15 11:33:56'),
(6,	'',	'Pay Bill',	'MCF61H49QO',	'John',	'J.',	'Doe',	'254708374149',	1,	'2609-06-28 14:05:14',	50459,	'600366',	'254708374149',	'',	'',	'2018-03-15 11:37:05',	'2018-03-15 11:37:05'),
(7,	'127.0.0.1',	'Pay Bill',	'MCF21H49TC',	'John',	'J.',	'Doe',	'254708374149',	1,	'2018-03-15 14:23:13',	50460,	'600366',	'254708374149',	'',	'',	'2018-03-15 14:23:15',	'2018-03-15 14:23:15'),
(8,	'127.0.0.1',	'Pay Bill',	'MCF51H49TF',	'John',	'J.',	'Doe',	'254708374149',	1,	'2018-03-15 14:29:34',	50461,	'600366',	'254708374149',	'',	'',	'2018-03-15 14:29:36',	'2018-03-15 14:29:36'),
(9,	'127.0.0.1',	'Pay Bill',	'MCF01H49TK',	'John',	'J.',	'Doe',	'254708374149',	1,	'2018-03-15 14:35:00',	50462,	'600366',	'254708374149',	'',	'',	'2018-03-15 14:35:03',	'2018-03-15 14:35:03');

DROP TABLE IF EXISTS `lnm_mpesa_request`;
CREATE TABLE `lnm_mpesa_request` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ipAddress` varchar(200) NOT NULL,
  `phoneNumber` varchar(200) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `accountRef` varchar(200) DEFAULT NULL,
  `transactionDesc` varchar(200) DEFAULT NULL,
  `resultUrl` varchar(200) DEFAULT NULL,
  `requestStatus` varchar(200) NOT NULL,
  `merchantRequestID` varchar(200) DEFAULT NULL,
  `checkoutRequestID` varchar(200) DEFAULT NULL,
  `responseDescription` varchar(200) NOT NULL,
  `responseCode` int(11) DEFAULT NULL,
  `customerMessage` varchar(200) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lnm_mpesa_request` (`id`, `ipAddress`, `phoneNumber`, `amount`, `accountRef`, `transactionDesc`, `resultUrl`, `requestStatus`, `merchantRequestID`, `checkoutRequestID`, `responseDescription`, `responseCode`, `customerMessage`, `createdAt`, `updatedAt`) VALUES
(1,	'0:0:0:0:0:0:0:1',	'+254716360959',	1,	'ygyuewuw\'111111',	'Payment for service',	'',	'LNM_TRANS',	'',	'',	'Some parameter is missing',	0,	'',	'2018-03-13 15:44:11',	'2018-03-13 15:44:11'),
(2,	'0:0:0:0:0:0:0:1',	'+254716360959',	1,	'ygyuewuw\'111112',	'',	'http://localhost:8080/boxbilling/mpesaApi/v2/lipanampesa',	'LNM_TRANS',	'',	'',	'Some parameter is missing',	0,	'',	'2018-03-13 15:45:26',	'2018-03-13 15:45:26'),
(3,	'0:0:0:0:0:0:0:1',	'+254716360959',	1,	'ygyuewuw\'111113',	'',	'http://localhost:8080/boxbilling/mpesaApi/v2/lipanampesa',	'LNM_TRANS',	'',	'',	'Some parameter is missing',	0,	'',	'2018-03-13 15:52:54',	'2018-03-13 15:52:54'),
(5,	'0:0:0:0:0:0:0:1',	'+254716360959',	1,	'ygyuewuw\'111156',	'payment for services',	'http://localhost:8080/boxbilling/mpesaApi/v2/lipanampesa',	'SUCCESS',	'',	'',	'Filed to send request',	0,	'',	'2018-03-14 11:49:00',	'2018-03-14 11:49:00'),
(6,	'0:0:0:0:0:0:0:1',	'+254716360959',	1,	'ygyuewuw\'111157',	'payment for services',	'http://localhost:8080/boxbilling/mpesaApi/v2/lipanampesa',	'SUCCESS',	'',	'',	'Failed to send request. Cant reach Safaricom',	0,	'',	'2018-03-14 12:02:56',	'2018-03-14 12:02:56'),
(13,	'0:0:0:0:0:0:0:1',	'+254716360959',	1,	'ygyuewuw\'111157dup0',	'payment for services',	'http://localhost:8080/boxbilling/mpesaApi/v2/lipanampesa',	'FAIL',	'',	'',	'Account reference number already exists',	0,	'',	'2018-03-15 16:08:16',	'2018-03-15 16:08:16'),
(15,	'0:0:0:0:0:0:0:1',	'+254716360959',	1,	'ygyuewuw\'111157dup',	'payment for services',	'http://localhost:8080/boxbilling/mpesaApi/v2/lipanampesa',	'FAIL',	'',	'',	'Account reference number already exists',	0,	'',	'2018-03-15 16:23:44',	'2018-03-15 16:23:44'),
(16,	'0:0:0:0:0:0:0:1',	'+254716360959',	1,	'ygyuewuw\'111157dup',	'payment for services',	'http://localhost:8080/boxbilling/mpesaApi/v2/lipanampesa',	'FAIL',	'',	'',	'Account reference number already exists',	0,	'',	'2018-03-15 16:23:49',	'2018-03-15 16:23:49'),
(17,	'0:0:0:0:0:0:0:1',	'+254716360959',	1,	'ygyuewuw\'111166',	'payment for services',	'http://localhost:8080/boxbilling/mpesaApi/v2/lipanampesa',	'SUCCESS',	'',	'',	'Failed to send request. Cant reach Safaricom',	0,	'',	'2018-03-15 16:28:38',	'2018-03-15 16:28:38'),
(18,	'0:0:0:0:0:0:0:1',	'+254716360959',	1,	'ygyuewuw\'111144',	'payment for services',	'http://localhost:8080/boxbilling/mpesaApi/v2/lipanampesa',	'SUCCESS',	'16680-3314765-2',	'ws_CO_15032018163215038',	'Success. Request accepted for processing',	0,	'Success. Request accepted for processing',	'2018-03-15 16:32:18',	'2018-03-15 16:32:18'),
(19,	'0:0:0:0:0:0:0:1',	'+254716360959',	1,	'ygyuewuw\'111199',	'payment for services',	'http://localhost:8080/boxbilling/mpesaApi/v2/lipanampesa',	'SUCCESS',	'25796-3303073-2',	'ws_CO_15032018163302695',	'Success. Request accepted for processing',	0,	'Success. Request accepted for processing',	'2018-03-15 16:33:05',	'2018-03-15 16:33:05'),
(20,	'0:0:0:0:0:0:0:1',	'+254716360959',	1,	'ygyuewuw\'111189',	'payment for services',	'http://localhost:8080/boxbilling/mpesaApi/v2/lipanampesa',	'SUCCESS',	'25794-3303574-2',	'ws_CO_15032018163548010',	'Success. Request accepted for processing',	0,	'Success. Request accepted for processing',	'2018-03-15 16:35:52',	'2018-03-15 16:35:52'),
(21,	'0:0:0:0:0:0:0:1',	'+254714687749',	1,	'CPL0001',	'payment for services',	'http://localhost:8080/boxbilling/mpesaApi/v2/lipanampesa',	'SUCCESS',	'16680-3537871-2',	'ws_CO_16032018155504016',	'Success. Request accepted for processing',	0,	'Success. Request accepted for processing',	'2018-03-16 15:55:07',	'2018-03-16 15:55:07'),
(22,	'0:0:0:0:0:0:0:1',	'+254716360959',	1,	'CPL0002',	'payment for services',	'http://localhost:8080/boxbilling/mpesaApi/v2/lipanampesa',	'SUCCESS',	'16679-4158314-2',	'ws_CO_19032018105029352',	'Success. Request accepted for processing',	0,	'Success. Request accepted for processing',	'2018-03-19 10:50:43',	'2018-03-19 10:50:43'),
(23,	'0:0:0:0:0:0:0:1',	'+254716360959',	1,	'CPL0003',	'payment for services',	'http://localhost:8080/boxbilling/mpesaApi/v2/lipanampesa',	'SUCCESS',	'17380-141298-2',	'ws_CO_20032018124617963',	'Success. Request accepted for processing',	0,	'Success. Request accepted for processing',	'2018-03-20 12:46:35',	'2018-03-20 12:46:35'),
(24,	'0:0:0:0:0:0:0:1',	'+254716360959',	1,	'CPL0004',	'payment for services',	'http://localhost:8081/paymentgateway/mpesa/res/lipanampesa',	'SUCCESS',	'17377-145989-2',	'ws_CO_20032018131647590',	'Success. Request accepted for processing',	0,	'Success. Request accepted for processing',	'2018-03-20 13:17:05',	'2018-03-20 13:17:05'),
(25,	'0:0:0:0:0:0:0:1',	'+254716360959',	1,	'CPL0005',	'payment for services',	'http://localhost:8081/paymentgateway/mpesa/res/lipanampesa',	'SUCCESS',	'16682-4557175-2',	'ws_CO_21032018113345699',	'Success. Request accepted for processing',	0,	'Success. Request accepted for processing',	'2018-03-21 11:33:47',	'2018-03-21 11:33:47'),
(26,	'0:0:0:0:0:0:0:1',	'+254716360959',	1,	'CPL0006',	'payment for services',	'http://localhost:8081/paymentgateway/mpesa/res/lipanampesa',	'SUCCESS',	'12363-1582110-2',	'ws_CO_27032018094354356',	'Success. Request accepted for processing',	0,	'Success. Request accepted for processing',	'2018-03-27 09:43:55',	'2018-03-27 09:43:55');

DROP TABLE IF EXISTS `lnm_mpesa_transaction`;
CREATE TABLE `lnm_mpesa_transaction` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ipAddress` varchar(200) NOT NULL,
  `merchantRequestID` varchar(100) NOT NULL,
  `checkoutRequestID` varchar(100) NOT NULL,
  `resultCode` int(11) NOT NULL,
  `resultDesc` varchar(200) NOT NULL,
  `phoneNumber` varchar(100) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `mpesaReceiptNumber` varchar(100) DEFAULT NULL,
  `balance` double DEFAULT NULL,
  `transactionDate` datetime DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `mpesaReceiptNumber` (`mpesaReceiptNumber`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `lnm_mpesa_transaction` (`id`, `ipAddress`, `merchantRequestID`, `checkoutRequestID`, `resultCode`, `resultDesc`, `phoneNumber`, `amount`, `mpesaReceiptNumber`, `balance`, `transactionDate`, `createdAt`, `updatedAt`) VALUES
(1,	'',	'8053-472355-2',	'ws_CO_23022018114424116',	0,	'The service request is processed successfully.',	'254712291940',	1,	'MBN7W5Z5QJ',	0,	'2018-02-23 11:44:37',	'2018-03-02 11:12:33',	'2018-03-02 11:12:33'),
(2,	'',	'25797-2067996-2',	'ws_CO_08032018153010354',	0,	'The service request is processed successfully.',	'254716360959',	1,	'MC855SRZ6J',	0,	'2018-03-08 15:30:29',	'2018-03-08 15:30:31',	'2018-03-08 15:30:31'),
(3,	'',	'16680-2379510-2',	'ws_CO_09032018152624672',	1101,	'[STK_CB - ]Invalid Input parameter \'Prompt message prefix\', length should be less than 94 characters',	NULL,	0,	NULL,	0,	NULL,	'2018-03-09 15:26:34',	'2018-03-09 15:26:34'),
(4,	'',	'16685-3127416-2',	'ws_CO_14032018114656008',	1101,	'[STK_CB - ]Invalid Input parameter \'Prompt message prefix\', length should be less than 94 characters',	NULL,	0,	NULL,	0,	NULL,	'2018-03-14 11:47:20',	'2018-03-14 11:47:20'),
(5,	'127.0.0.1',	'16680-3314765-2',	'ws_CO_15032018163215038',	1101,	'[STK_CB - ]Invalid Input parameter \'Prompt message prefix\', length should be less than 94 characters',	NULL,	NULL,	NULL,	NULL,	NULL,	'2018-03-15 16:32:19',	'2018-03-15 16:32:19'),
(6,	'127.0.0.1',	'25796-3303073-2',	'ws_CO_15032018163302695',	1101,	'[STK_CB - ]Invalid Input parameter \'Prompt message prefix\', length should be less than 94 characters',	NULL,	NULL,	NULL,	NULL,	NULL,	'2018-03-15 16:33:06',	'2018-03-15 16:33:06'),
(7,	'127.0.0.1',	'25794-3303574-2',	'ws_CO_15032018163548010',	0,	'The service request is processed successfully.',	'254716360959',	1,	'MCF3AFTRJN',	0,	'2018-03-15 16:36:04',	'2018-03-15 16:36:07',	'2018-03-15 16:36:07'),
(8,	'127.0.0.1',	'16680-3537871-2',	'ws_CO_16032018155504016',	0,	'The service request is processed successfully.',	'254714687749',	1,	'MCG5B1NP1T',	0,	'2018-03-16 15:55:46',	'2018-03-16 15:55:48',	'2018-03-16 15:55:48'),
(9,	'127.0.0.1',	'16679-4158314-2',	'ws_CO_19032018105029352',	0,	'The service request is processed successfully.',	'254716360959',	1,	'MCJ1CRF64H',	0,	'2018-03-19 10:50:57',	'2018-03-19 10:50:59',	'2018-03-19 10:50:59'),
(10,	'127.0.0.1',	'17380-141298-2',	'ws_CO_20032018124617963',	0,	'The service request is processed successfully.',	'254716360959',	1,	'MCK8DFM1XY',	0,	'2018-03-20 12:47:10',	'2018-03-20 12:47:12',	'2018-03-20 12:47:12'),
(11,	'127.0.0.1',	'17377-145989-2',	'ws_CO_20032018131647590',	0,	'The service request is processed successfully.',	'254716360959',	1,	'MCK1DG9DPZ',	0,	'2018-03-20 13:17:54',	'2018-03-20 13:17:57',	'2018-03-20 13:17:57'),
(12,	'127.0.0.1',	'16682-4557175-2',	'ws_CO_21032018113345699',	1032,	'[STK_CB - ]Request cancelled by user',	NULL,	NULL,	NULL,	NULL,	NULL,	'2018-03-21 11:34:01',	'2018-03-21 11:34:01');

DROP TABLE IF EXISTS `mpesa_Request`;
CREATE TABLE `mpesa_Request` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ipAddress` varchar(200) NOT NULL,
  `requestDetails` longtext NOT NULL,
  `requestType` varchar(200) NOT NULL,
  `requestStatus` varchar(200) NOT NULL,
  `requestResult` longtext,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `mpesa_Request` (`id`, `ipAddress`, `requestDetails`, `requestType`, `requestStatus`, `requestResult`, `createdAt`, `updatedAt`) VALUES
(1,	'0:0:0:0:0:0:0:1',	'{\"TransactionType\":\"CustomerPayBillOnline\",\"Amount\":1,\"CallBackURL\":\"http://b0277c51.ngrok.io/paymentgateway/mpesaapi/v1/lipanampesa/callback\",\"PhoneNumber\":\"254716360959\",\"PartyA\":\"254716360959\",\"PartyB\":\"174379\",\"AccountReference\":\"ygyuewuw\'111111\",\"TransactionDesc\":\"\\tPayment for service\",\"BusinessShortCode\":\"174379\",\"Timestamp\":\"20180308152947\",\"Password\":\"MTc0Mzc5YmZiMjc5ZjlhYTliZGJjZjE1OGU5N2RkNzFhNDY3Y2QyZTBjODkzMDU5YjEwZjc4ZTZiNzJhZGExZWQyYzkxOTIwMTgwMzA4MTUyOTQ3\"}',	'LNM_TRANS',	'SUCCESS',	'{\n            \"MerchantRequestID\":\"16682-2127197-2\",\n            \"CheckoutRequestID\":\"ws_CO_08032018152942312\",\n            \"ResponseCode\": \"0\",\n            \"ResponseDescription\":\"Success. Request accepted for processing\",\n            \"CustomerMessage\":\"Success. Request accepted for processing\"\n        }\n        ',	'2018-03-08 15:29:48',	'2018-03-08 15:29:48'),
(2,	'0:0:0:0:0:0:0:1',	'{\"TransactionType\":\"CustomerPayBillOnline\",\"Amount\":1,\"CallBackURL\":\"http://b0277c51.ngrok.io/paymentgateway/mpesaapi/v1/lipanampesa/callback\",\"PhoneNumber\":\"254716360959\",\"PartyA\":\"254716360959\",\"PartyB\":\"174379\",\"AccountReference\":\"ygyuewuw\'111111\",\"TransactionDesc\":\"\\tPayment for service\",\"BusinessShortCode\":\"174379\",\"Timestamp\":\"20180308153015\",\"Password\":\"MTc0Mzc5YmZiMjc5ZjlhYTliZGJjZjE1OGU5N2RkNzFhNDY3Y2QyZTBjODkzMDU5YjEwZjc4ZTZiNzJhZGExZWQyYzkxOTIwMTgwMzA4MTUzMDE1\"}',	'LNM_TRANS',	'SUCCESS',	'{\n            \"MerchantRequestID\":\"25797-2067996-2\",\n            \"CheckoutRequestID\":\"ws_CO_08032018153010354\",\n            \"ResponseCode\": \"0\",\n            \"ResponseDescription\":\"Success. Request accepted for processing\",\n            \"CustomerMessage\":\"Success. Request accepted for processing\"\n        }\n        ',	'2018-03-08 15:30:16',	'2018-03-08 15:30:16'),
(3,	'0:0:0:0:0:0:0:1',	'{\n  \"phoneNumber\": \"+25471636095\",\n  \"amount\": 1,\n  \"accRef\": \"ygyuewuw\'111111\",\n  \"transactionDesc\":\"	Payment for service\"\n}',	'LNM_TRANS',	'FAIL',	'Invalid Phone number',	'2018-03-08 15:34:03',	'2018-03-08 15:34:03'),
(4,	'0:0:0:0:0:0:0:1',	'{\n  \"phoneNumber\": \"+25471636095\",\n  \"amount\": 1,\n  \"accRef\": \"ygyuewuw\'111111\",\n  \"transactionDesc\":\"	Payment for service\"\n}',	'LNM_TRANS',	'FAIL',	'Invalid Phone number',	'2018-03-09 14:47:29',	'2018-03-09 14:47:29'),
(5,	'0:0:0:0:0:0:0:1',	'{\n  \"phoneNumber\": \"+25471636095o\",\n  \"amount\": 1,\n  \"accRef\": \"ygyuewuw\'111111\",\n  \"transactionDesc\":\"	Payment for service\"\n}',	'LNM_TRANS',	'FAIL',	'Invalid Phone number',	'2018-03-09 14:47:55',	'2018-03-09 14:47:55'),
(6,	'0:0:0:0:0:0:0:1',	'{\n  \"phoneNumber\": \"\",\n  \"amount\": 1,\n  \"accRef\": \"ygyuewuw\'111111\",\n  \"transactionDesc\":\"	Payment for service\"\n}',	'LNM_TRANS',	'FAIL',	'Phone Number not entered',	'2018-03-09 14:48:14',	'2018-03-09 14:48:14'),
(7,	'0:0:0:0:0:0:0:1',	'{\n  \"phoneNumber\": \"+254716360959\",\n  \"amount\": 1,\n  \"accRef\": \"ygyuewuw\'111111\",\n  \"transactionDesc\":\"	Payment for service\"\n}',	'LNM_TRANS',	'SUCCESS',	'{\n            \"MerchantRequestID\":\"25797-2310468-2\",\n            \"CheckoutRequestID\":\"ws_CO_09032018144917247\",\n            \"ResponseCode\": \"0\",\n            \"ResponseDescription\":\"Success. Request accepted for processing\",\n            \"CustomerMessage\":\"Success. Request accepted for processing\"\n        }\n        ',	'2018-03-09 14:49:26',	'2018-03-09 14:49:26'),
(8,	'0:0:0:0:0:0:0:1',	'{\n  \"phoneNumber\": \"+254716360959\",\n  \"amount\": 1,\n  \"accRef\": \"ygyuewuw\'111111\",\n  \"transactionDesc\":\"	Payment for service\"\n}',	'LNM_TRANS',	'SUCCESS',	'{\n            \"MerchantRequestID\":\"13595-43033-2\",\n            \"CheckoutRequestID\":\"ws_CO_09032018145022265\",\n            \"ResponseCode\": \"0\",\n            \"ResponseDescription\":\"Success. Request accepted for processing\",\n            \"CustomerMessage\":\"Success. Request accepted for processing\"\n        }\n        ',	'2018-03-09 14:50:31',	'2018-03-09 14:50:31'),
(9,	'0:0:0:0:0:0:0:1',	'{\n  \"phoneNumber\": \"+254716360959\",\n  \"amount\": 1,\n  \"accRef\": \"ygyuewuw\'111111\",\n  \"transactionDesc\":\"	Payment for service\"\n}',	'LNM_TRANS',	'SUCCESS',	'{\n            \"MerchantRequestID\":\"25794-2311456-2\",\n            \"CheckoutRequestID\":\"ws_CO_09032018145523699\",\n            \"ResponseCode\": \"0\",\n            \"ResponseDescription\":\"Success. Request accepted for processing\",\n            \"CustomerMessage\":\"Success. Request accepted for processing\"\n        }\n        ',	'2018-03-09 14:55:32',	'2018-03-09 14:55:32'),
(10,	'0:0:0:0:0:0:0:1',	'{\n  \"phoneNumber\": \"+254716360959\",\n  \"amount\": 1,\n  \"accRef\": \"ygyuewuw\'111111\",\n  \"transactionDesc\":\"	Payment for service\"\n}',	'LNM_TRANS',	'SUCCESS',	'{\n            \"MerchantRequestID\":\"16680-2379510-2\",\n            \"CheckoutRequestID\":\"ws_CO_09032018152624672\",\n            \"ResponseCode\": \"0\",\n            \"ResponseDescription\":\"Success. Request accepted for processing\",\n            \"CustomerMessage\":\"Success. Request accepted for processing\"\n        }\n        ',	'2018-03-09 15:26:33',	'2018-03-09 15:26:33'),
(11,	'0:0:0:0:0:0:0:1',	'{\n  \"shortCode\": \"600000\",\n  \"amount\": 10\n}',	'B2B_TRANS',	'SUCCESS',	'{\n            \"OriginatorConversationID\": \"13602-824583-2\",\n            \"ConversationID\": \"AG_20180314_000046e6b94d053361cc\",\n            \"ResponseCode\":\"0\",\n            \"ResponseDescription\": \"Accept the service request successfully.\"\n            \n        }\n        ',	'2018-03-14 11:22:06',	'2018-03-14 11:22:06'),
(12,	'0:0:0:0:0:0:0:1',	'{\n  \"phoneNumber\": \"+254716360959\",\n  \"amount\": 1,\n  \"accRef\": \"ygyuewuw\'111234\",\n  \"transactionDesc\":\"	Payment for service\"\n}',	'LNM_TRANS',	'SUCCESS',	'{\n            \"MerchantRequestID\":\"16685-3127416-2\",\n            \"CheckoutRequestID\":\"ws_CO_14032018114656008\",\n            \"ResponseCode\": \"0\",\n            \"ResponseDescription\":\"Success. Request accepted for processing\",\n            \"CustomerMessage\":\"Success. Request accepted for processing\"\n        }\n        ',	'2018-03-14 11:47:19',	'2018-03-14 11:47:19'),
(13,	'0:0:0:0:0:0:0:1',	'{\n  \"phoneNumber\": \"+254716360959\",\n  \"amount\": 1,\n  \"accRef\": \"ygyuewuw\'111234\",\n  \"transactionDesc\":\"	Payment for service\"\n}',	'LNM_TRANS',	'SUCCESS',	'{\"response\":\"Failed to send request. Cant reach Safaricom\"}',	'2018-03-14 12:07:32',	'2018-03-14 12:07:32'),
(14,	'0:0:0:0:0:0:0:1',	'{\n  \"shortCode\": \"60000\",\n  \"amount\": 10\n}',	'B2B_TRANS',	'SUCCESS',	'{\"response\":\"Failed to send request. Cant reach Safaricom\"}',	'2018-03-14 16:18:35',	'2018-03-14 16:18:35'),
(15,	'0:0:0:0:0:0:0:1',	'{\n  \"shortCode\": \"600000\",\n  \"amount\": 10\n}',	'B2B_TRANS',	'SUCCESS',	'{\n            \"OriginatorConversationID\": \"25797-3141529-2\",\n            \"ConversationID\": \"AG_20180314_00006141e47396b69675\",\n            \"ResponseCode\":\"0\",\n            \"ResponseDescription\": \"Accept the service request successfully.\"\n            \n        }\n        ',	'2018-03-14 16:18:51',	'2018-03-14 16:18:51'),
(16,	'0:0:0:0:0:0:0:1',	'{\n  \"shortCode\": \"60000\",\n  \"amount\": 10\n}',	'B2B_TRANS',	'SUCCESS',	'{\n            \"OriginatorConversationID\": \"25797-3142096-2\",\n            \"ConversationID\": \"AG_20180314_00005303dddd4fc79420\",\n            \"ResponseCode\":\"0\",\n            \"ResponseDescription\": \"Accept the service request successfully.\"\n            \n        }\n        ',	'2018-03-14 16:23:19',	'2018-03-14 16:23:19'),
(17,	'0:0:0:0:0:0:0:1',	'{\n  \"shortCode\": \"60000\",\n  \"amount\": 10\n}',	'B2B_TRANS',	'SUCCESS',	'{\n            \"OriginatorConversationID\": \"25797-3260743-2\",\n            \"ConversationID\": \"AG_20180315_0000642e90b1ca05ed6b\",\n            \"ResponseCode\":\"0\",\n            \"ResponseDescription\": \"Accept the service request successfully.\"\n            \n        }\n        ',	'2018-03-15 11:13:23',	'2018-03-15 11:13:23'),
(18,	'0:0:0:0:0:0:0:1',	'{\n  \"shortCode\": \"60000\",\n  \"amount\": 10\n}',	'B2B_TRANS',	'SUCCESS',	'{\"response\":\"Failed to send request. Cant reach Safaricom\"}',	'2018-03-15 11:19:13',	'2018-03-15 11:19:13'),
(19,	'0:0:0:0:0:0:0:1',	'{\n  \"shortCode\": \"600000\",\n  \"amount\": 10\n}',	'B2B_TRANS',	'SUCCESS',	'{\n            \"OriginatorConversationID\": \"13597-983992-2\",\n            \"ConversationID\": \"AG_20180315_0000724d63e1603f3380\",\n            \"ResponseCode\":\"0\",\n            \"ResponseDescription\": \"Accept the service request successfully.\"\n            \n        }\n        ',	'2018-03-15 11:20:00',	'2018-03-15 11:20:00'),
(20,	'0:0:0:0:0:0:0:1',	'{\n  \"shortCode\": \"60000\",\n  \"amount\": 10\n}',	'B2B_TRANS',	'SUCCESS',	'{\n            \"OriginatorConversationID\": \"25796-3261752-2\",\n            \"ConversationID\": \"AG_20180315_00004227997f32063497\",\n            \"ResponseCode\":\"0\",\n            \"ResponseDescription\": \"Accept the service request successfully.\"\n            \n        }\n        ',	'2018-03-15 11:21:58',	'2018-03-15 11:21:58'),
(21,	'0:0:0:0:0:0:0:1',	'{\n  \"shortCode\": \"60000\",\n  \"amount\": 10\n}',	'B2B_TRANS',	'SUCCESS',	'{\n            \"OriginatorConversationID\": \"25798-3261889-2\",\n            \"ConversationID\": \"AG_20180315_00005513ca3de9b92b14\",\n            \"ResponseCode\":\"0\",\n            \"ResponseDescription\": \"Accept the service request successfully.\"\n            \n        }\n        ',	'2018-03-15 11:23:12',	'2018-03-15 11:23:12'),
(22,	'0:0:0:0:0:0:0:1',	'{\n  \"shortCode\": \"60000\",\n  \"amount\": 10\n}',	'B2B_TRANS',	'SUCCESS',	'{\n            \"OriginatorConversationID\": \"13588-984938-2\",\n            \"ConversationID\": \"AG_20180315_000056705278ac276be3\",\n            \"ResponseCode\":\"0\",\n            \"ResponseDescription\": \"Accept the service request successfully.\"\n            \n        }\n        ',	'2018-03-15 11:27:34',	'2018-03-15 11:27:34');

-- 2018-03-29 07:29:10
